# computer-store-assignment

[![standard-readme compliant](https://img.shields.io/badge/standard--readme-OK-green.svg?style=flat-square)](https://github.com/RichardLitt/standard-readme)

Javascript assignment 1. Contains three sections interacting with each other using vanilla Javascript. The HTML is build up using Bootstrap 5 and flexbox. The three sections are as followed: A bank section to store a currency called Virtual Tostis (VT) and to get a loan. A work section to earn more VT, move them to the bank or pay of your loan should you have one. And a laptop section to view several section and possible buy one if you have enough VT. The laptops are fetched from an API.

## Table of Contents

- [Install](#install)
- [Usage](#usage)
- [API](#api)
- [Maintainers](#maintainers)
- [License](#license)

## Install

Download as .zip or fork the project. Then either open the index.html in you favorite browser.

## Usage

You can press the Work button to gain VT, which can be moved to the bank using Deposit to Bank button. Should you have a loan, then 10% will be deducted from the Salary and used to pay of the loan.

The Get Loan button opens up a prompt which asks how much you which to receive. This amount must be a number and cannot exceed double the amount of VT currently stored in the bank. You can only get one loan. If you want another, you'll have to pay off your first one.

The Pay loan button does this: All VT in currently worked up as Salary will be used to Pay of the loan. Anything left over will be added to the bank.

The dropdown menu shows several laptops. Selected one will give you more information.

The buy laptop button allows you to attempt to buy the laptop. This is only possible if you have enough VT stored in the bank. If you do, the cost of the laptop will be deducted from the balance.

## API
"https://noroff-komputer-store-api.herokuapp.com/computers" is used for the laptops


## Maintainers

[@rikdortmans](https://github.com/rikdortmans)


## License

MIT © 2022 Rik Dortmans
